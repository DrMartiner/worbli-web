import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule, Routes} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from "@angular/common/http";
import {WebStorageModule} from 'ngx-store';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {IsAuthService, UserService} from "./app.service";
import {AlertService} from "./services";

import {AppComponent} from './app.component';
import {HomeComponent} from './apps/pages/home/home.component';
import {NotFoundComponent} from './apps/pages/not-found/not-found.component';
import {AppGuard} from "./app.guard";
import {AboutComponent} from './apps/pages/about/about.component';
import {TeamComponent} from './apps/pages/team/team.component';
import {RoadmapComponent} from './apps/pages/roadmap/roadmap.component';
import {NotifyModalContent} from "./apps/shared/notify-modal.component";
import {SignUpModalContent} from "./apps/shared/signup-modal.component";
import { SignUpCompleteComponent } from './apps/auth/sign-up-complete/sign-up-complete.component';
import { EosAccountComponent } from './apps/user/eos-account/eos-account.component';
import { ChangePasswordComponent } from './apps/user/change-password/change-password.component';
import { SignInModalContent } from "./apps/shared/signin-modal.component";
import { DashboardComponent } from './apps/user/dashboard/dashboard.component';
import { KycModalContent } from "./apps/user/dashboard/kyc-modal.component";
import { SettingsComponent } from './apps/user/settings/settings.component';
import { SignOutModalContent } from "./apps/shared/signout-modal.component";

const appRoutes: Routes =[
    { path: '', component: HomeComponent, pathMatch: 'full' },
    { path: 'about', component: AboutComponent, pathMatch: 'full' },
    { path: 'roadmap', component: RoadmapComponent, pathMatch: 'full' },
    { path: 'team', component: TeamComponent, pathMatch: 'full' },

    { path: 'auth/sign-up/complete', component: SignUpCompleteComponent},
    // { path: 'auth/reset-password', component: ResetPasswordComponent},
    // { path: 'auth/reset-password/complete', component: ResetPasswordComponent},

    { path: 'user/change-password', component: ChangePasswordComponent, canActivate: [AppGuard]},
    { path: 'user/settings', component: SettingsComponent, canActivate: [AppGuard]},
    { path: 'user/eos-account', component: EosAccountComponent, canActivate: [AppGuard]},
    { path: 'user/dashboard', component: DashboardComponent, canActivate: [AppGuard]},

    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [
        BrowserModule,
        WebStorageModule,
        ReactiveFormsModule,
        RouterModule.forRoot(appRoutes),
        HttpClientModule,
        NgbModule.forRoot(),
    ],
    providers: [
        AppGuard,
        AlertService,
        UserService,
        IsAuthService,
    ],
    entryComponents: [
        NotifyModalContent,
        SignUpModalContent,
        SignInModalContent,
        SignOutModalContent,
        KycModalContent,
    ],
    declarations: [
        AppComponent,

        NotifyModalContent,
        SignUpModalContent,
        SignInModalContent,
        SignOutModalContent,

        HomeComponent,
        NotFoundComponent,
        AboutComponent,
        TeamComponent,
        RoadmapComponent,

        SignUpCompleteComponent,

        EosAccountComponent,
        ChangePasswordComponent,
        DashboardComponent,
        KycModalContent,
        SettingsComponent,
    ],
    bootstrap: [AppComponent, ]
})
export class AppModule { }
