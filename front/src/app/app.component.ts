import {Component, OnInit} from '@angular/core';
import {IsAuthService, UserService} from "./app.service";
import { Alert, AlertService, AlertType } from "./services";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {SignUpModalContent} from "./apps/shared/signup-modal.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {LocalStorageService} from "ngx-store";
import {SignInModalContent} from "./apps/shared/signin-modal.component";
import {SignOutModalContent} from "./apps/shared/signout-modal.component";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    public alerts: Alert[] = [];

    constructor(
        private http: HttpClient,
        public router: Router,
        private modalService: NgbModal,
        private alertService: AlertService,
        public userService: UserService,
        public isAuthService: IsAuthService,
        private localStorageService: LocalStorageService,
    ) { }

    ngOnInit() {
        this.checkSessionid();

        this.alertService.getAlert().subscribe((alert: Alert) => {
            if (!alert) {
                this.alerts = [];
            }
            else {
                this.alerts.push(alert);
                setTimeout(() => this.removeAlert(alert), 4000);
            }
        });
    }

    public signOut(): void {
        this.modalService.open(SignOutModalContent);
    }

    private checkSessionid(): void {
        this.http.post('/api/v1/auth/check-sessionid/', {})
            .subscribe((response) => {
                if (response['payload']['is_active']) {
                    this.localStorageService.set('isAuth', true);
                }
                else {
                    this.alertService.warn('Your account is not activated. Check your email to activate it.');
                    this.localStorageService.remove('isAuth');
                    this.router.navigate(['/']);
                }
            }, (error) => {
                if (error.status == 404 || error.status == 400) {
                    this.localStorageService.remove('isAuth');
                }
            });
    }

    public openSignUpPopup(): void {
        this.modalService.open(SignUpModalContent);
    }

    public openSignInModal(): void {
        this.modalService.open(SignInModalContent);
    }

    public removeAlert(alert: Alert) {
        this.alerts = this.alerts.filter(x => x !== alert);
    }

    public getAlertType(alert: Alert) {
        if (!alert) {
            return AlertType.Success;
        }

        switch (alert.type) {
            case AlertType.Success:
                return 'success';
            case AlertType.Error:
                return 'danger';
            case AlertType.Info:
                return 'info';
            case AlertType.Warning:
                return 'warning';
        }
        return '';
    }
}
