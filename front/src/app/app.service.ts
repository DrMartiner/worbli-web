import {Injectable} from '@angular/core';
import {LocalStorageService} from "ngx-store";
import {HttpClient} from "@angular/common/http";

@Injectable({providedIn: 'root'})
export class IsAuthService {
    constructor(private localStorageService: LocalStorageService) {}

    public isAuth(): boolean {
        return this.localStorageService.get('isAuth');
    }
}

@Injectable({providedIn: 'root'})
export class UserService {
    private userProfile: Object = {};

    constructor(private http: HttpClient) {}

    public getProfile(): Object {
        this.loadProfile();

        return this.userProfile;
    }

    private loadProfile(): void {
        this.http.get('/api/v1/user/self')
            .subscribe((response) => {
                this.userProfile = response['payload']
            }, (error) => {});
    }
}
