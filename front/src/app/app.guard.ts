import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs";
import {Injectable} from "@angular/core";
import {IsAuthService} from "./app.service";

@Injectable()
export class AppGuard implements CanActivate{
    constructor(private isAuthService: IsAuthService) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : Observable<boolean> | boolean {
        return this.isAuthService.isAuth();
    }
}