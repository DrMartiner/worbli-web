import { Injectable } from '@angular/core';
import { Observable, Subject } from "rxjs/index";
import { Alert, AlertType } from './alert.model';

@Injectable()
export class AlertService {
    private subject = new Subject<Alert>();

    public getAlert(): Observable<any> {
        return this.subject.asObservable();
    }

    public success(message: any): void {
        this.emitMessage(AlertType.Success, message);
    }

    public error(message: any): void {
        this.emitMessage(AlertType.Error, message);
    }

    public info(message: any): void {
        this.emitMessage(AlertType.Info, message);
    }

    public warn(message: any): void {
        this.emitMessage(AlertType.Warning, message);
    }

    public alert(type: AlertType, message: any): void {
        this.subject.next(<Alert>{ type: type, message: message });
    }

    public clear(): void {
        this.subject.next();
    }

    private emitMessage(alertType, message: any): void {
        const messages = this.parseMessage(message);
        for (let message of messages)
            this.alert(alertType, message);
    }

    private parseMessage(message: any): Array<string> {
        if (typeof message == 'string')
            return [message];

        if (Array.isArray(message))
            return message;

        if (typeof message == 'object') {
            let messages: Array<string> = [];
            for (let key of Object.keys(message))
                messages = messages.concat(message[key]);
            return messages;
        }

        console.warn('Message is not String, Array or Object: ', message);
        return [];
    }
}
