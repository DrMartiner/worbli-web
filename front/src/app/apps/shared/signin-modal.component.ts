import {Component, Input} from "@angular/core";
import {NgbActiveModal, NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {SignUpModalContent} from "./signup-modal.component";
import {Router} from "@angular/router";
import {LocalStorageService} from "ngx-store";

@Component({
    selector: 'sign-in-modal-content',
    template: `
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" (click)="close()">
                <span aria-hidden="true"><img src="assets/img/close.png"></span>
            </button>
        </div>
        <div class="modal-body">
            <div class="modal-worbli-title">Sign In</div>
            <p>Please enter your email address and password</p>
            <form [formGroup]="form" (ngSubmit)="submit()">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email Address</label>
                    <input type="email" class="form-control" placeholder="Enter your email address" formControlName="email" required >
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Password</label>
                    <input type="password" class="form-control" placeholder="Enter your password" formControlName="password" required >
                </div>
                <!--div class="form-group form-check">
                    <input type="checkbox" class="form-check-input">
                    <label class="form-check-label">Remember me</label>
                </div-->
                <button type="submit" class="btn btn-primary btn-block" [disabled]="form.invalid">SIGN IN</button>
            </form>
            <br>
            <p>Not a member? <a (click)="openSignUpModal(); false" href="#">Join now</a></p>

            <div class="alert alert-warning" role="alert" *ngIf="errorMessage">
                {{ errorMessage }}
            </div>
        </div>
    `
})
export class SignInModalContent {
    @Input() title;
    @Input() text;

    public form: FormGroup;
    public errorMessage: String = null;

    constructor(
        private fb: FormBuilder,
        private http: HttpClient,
        private modalService: NgbModal,
        public activeModal: NgbActiveModal,
        private router: Router,
        private localStorageService: LocalStorageService,
    ) {
        this.form = fb.group({
            email : [null, [Validators.required, Validators.email]],
            password : [null, [Validators.required]],
        });
    }

    public submit(): void {
        this.errorMessage = null;

        this.http.post('/api/v1/auth/sign-in/', this.form.value)
            .subscribe((response) => {
                this.router.navigate(['/']);
                this.localStorageService.set('isAuth', true);
                this.activeModal.close();
            }, (error) => {
                switch(error.status) {
                    case 401: {
                        this.errorMessage = error['error'].message;
                        break;
                    }
                    case 404: {
                        this.errorMessage = error['error'].message;
                        break;
                    }
                    case 406: {
                        this.errorMessage = error['error'].message;
                        break;
                    }
                    case 410: {
                        this.errorMessage = error['error'].message;
                        break;
                    }
                    default: {
                        this.errorMessage = 'Error occurred.';
                        break;
                    }
                }
            });
    }

    public openSignUpModal(): void {
        this.activeModal.close();
        this.modalService.open(SignUpModalContent);
    }

    public close(): void {
        this.activeModal.close();
    }
}
