import {Component, Input} from "@angular/core";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'notify-modal-content',
    template: `
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" (click)="close()">
                <span aria-hidden="true"><img src="assets/img/close.png"></span>
            </button>
        </div>
        <div class="modal-body">
            <div class="modal-worbli-title">{{ title }}</div>
            <p *ngIf="text">{{ text }}</p>
            <div *ngIf="html" [innerHTML]="html"></div>
        </div>
    `
})
export class NotifyModalContent {
    @Input() title;
    @Input() text;
    @Input() html;

    constructor(public activeModal: NgbActiveModal) {}

    public close(): void {
        this.activeModal.close();
    }
}
