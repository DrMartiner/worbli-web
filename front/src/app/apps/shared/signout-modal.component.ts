import {Component, Input} from "@angular/core";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {NotifyModalContent} from "./notify-modal.component";
import {LocalStorageService} from "ngx-store";
import {AlertService} from "../../services";
import {Router} from "@angular/router";

@Component({
    selector: 'sign-up-modal-content',
    template: `
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" (click)="close()">
                <span aria-hidden="true"><img src="assets/img/close.png"></span>
            </button>
        </div>
        <div class="modal-body">
            <br>
            <button type="submit" class="btn btn-lg btn-info btn-block" (click)="signOut()">Sign-Out</button>
        </div>
    `
})
export class SignOutModalContent {
    constructor(
        private http: HttpClient,
        private router: Router,
        public activeModal: NgbActiveModal,
        private alertService: AlertService,
        private localStorageService: LocalStorageService,
    ) { }

    public signOut(): void {
        this.http.post('/api/v1/auth/sign-out/', {})
            .subscribe((response) => {
                this.localStorageService.remove('isAuth');
                this.activeModal.close();
                this.router.navigate(['/']);
            }, (error) => {
                this.alertService.error('Error occurred.');
            });
    }

    public close(): void {
        this.activeModal.close();
    }
}
