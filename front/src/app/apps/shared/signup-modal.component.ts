import {Component, Input} from "@angular/core";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {LocalStorageService} from "ngx-store";
import {Router} from "@angular/router";

@Component({
    selector: 'sign-up-modal-content',
    template: `
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" (click)="close()">
                <span aria-hidden="true"><img src="assets/img/close.png"></span>
            </button>
        </div>
        <div class="modal-body">
            <div class="modal-worbli-title">Join Now</div>

            <form [formGroup]="form" (ngSubmit)="submit()" *ngIf="!messageText">
                <p>Please enter your email address to join</p>
                <label for="id_email_sign_up_modal">Your Email Address</label>
                <input type="email" class="form-control" id="id_email_sign_up_modal" placeholder="Enter email" formControlName="email" required />
                <small class="form-text text-muted">By registering you agree to the Terms and  Conditions and opt-in to marketing communications</small>
                <button type="submit" class="btn btn-primary btn-block" [disabled]="form.invalid">JOIN NOW</button>
            </form>

            <hr *ngIf="!messageText" />
            <button type="button" class="btn btn-primary btn-block" (click)="signUpViaScatter()" *ngIf="!messageText">
                SING UP via Scatter
            </button>

            <hr *ngIf="messageText">
            <p *ngIf="messageText">{{ messageText }}</p>
        </div>
    `
})
export class SignUpModalContent {
    @Input() title;
    @Input() text;

    public form: FormGroup;
    public messageText: String = null;
    public hasScatterExtension = false;

    constructor(
        private fb: FormBuilder,
        private http: HttpClient,
        private router: Router,
        public activeModal: NgbActiveModal,
        private localStorageService: LocalStorageService,
    ) {
        this.form = fb.group({
            email : [null, [Validators.required, Validators.email]],
        });
    }

    ngOnInit() {
        window['scatter'].connect('Worbli App').then(connected => {
            if(connected) {
                this.hasScatterExtension = true;
            }
        });
    }

    public submit(): void {
        this.http.post('/api/v1/auth/sign-up/', this.form.value)
            .subscribe((response) => {
                this.messageText = `SignUp confirm code was sent to "${this.form.value['email']}".`;
            }, (error) => {
                switch(error.status) {
                    case 409: {
                        this.messageText = `Email "${this.form.value['email']}" is already exists.`;
                        break;
                    }
                    default: {
                        this.messageText = 'Error occurred.';
                        break;
                    }
                }
            });
    }

    public signUpViaScatter(): void {
        window['scatter'].connect('Worbli App').then(connected => {
            if(!connected) {
                return ;
            }

            window['scatter'].getIdentity({personal: ['firstname', 'lastname', 'email']}).then(identity => {
                console.log('identity', identity);

                window['scatter'].authenticate().then(scatter_hash => {
                    console.log('scatter_hash', scatter_hash);
                }).catch(error => {
                    console.log('scatter_hash - error', error);
                });
            }).catch(error => {
                console.log('getIdentity - error', error);
            });
        });
    }

    public close(): void {
        this.activeModal.close();
    }
}
