import {Component, Input} from "@angular/core";
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {LocalStorageService} from "ngx-store";
import {Router} from "@angular/router";
import {DomSanitizer} from "@angular/platform-browser";
import {AlertService} from "../../../services";

@Component({
    selector: 'kyc-modal-content',
    template: `
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" (click)="close()">
                <span aria-hidden="true"><img src="assets/img/close.png"></span>
            </button>
        </div>
        <div class="modal-body">
            <div class="modal-worbli-title">KYC</div>

            <p *ngIf="authenteqSvg">
                Please, scan the code to finish the verify:
            </p>
            <img [src]="authenteqSvg" *ngIf="authenteqSvg" />

            <form [formGroup]="form" (ngSubmit)="submit()" *ngIf="!authenteqSvg">
                <p>
                    Please enter your given name, surname and passport no to verify via
                    <a href="https://authenteq.com">authenteq.com</a> service.
                </p>
                <div class="form-group">
                    <label for="id_givenname">Given name</label>
                    <input type="text" class="form-control" id="id_old_password" formControlName="givenname" required>
                </div>
                <div class="form-group">
                    <label for="id_surname">Surname</label>
                    <input type="text" class="form-control" id="id_surname" formControlName="surname" required>
                </div>
                <div class="form-group">
                    <label for="id_passportno">Passport no</label>
                    <input type="text" class="form-control" id="id_passportno" formControlName="passportno" required>
                </div>
                <button type="submit" class="btn btn-primary btn-block" [disabled]="form.invalid">Verify</button>
            </form>
        </div>
    `
})
export class KycModalContent {
    public form: FormGroup;

    public authenteqSvg = null;

    constructor(
        private fb: FormBuilder,
        private http: HttpClient,
        public activeModal: NgbActiveModal,
        private domSanitizer: DomSanitizer,
        private alertService: AlertService,
    ) {
        this.form = fb.group({
            givenname : [null, Validators.required],
            surname : [null, Validators.required],
            passportno : [null, Validators.required],
        });
    }

    public submit(): void {
        window['Authenteq'].connect(
            'worbli-prod',
            'givenname,surname,passportno',
            (data: Object) => {
                const svgData = window['Authenteq'].createAQRSvg(data['svg']);
                this.authenteqSvg = this.domSanitizer.bypassSecurityTrustUrl(svgData);
            },
            (usertoken: String) => {
                this.activeModal.close();

                let postData = this.form.value;
                postData['usertoken'] = usertoken;

                this.http.post('/api/v1/kyc/checking/', postData)
                    .subscribe((response) => {
                        this.alertService.info('Data was sent to KYC checking. We will sent you a notify.');
                    }, (error) => {
                        this.alertService.error('Error occurred at sending data to KYC.');
                    });
            }
        );
    }

    public close(): void {
        this.activeModal.close();
    }
}
