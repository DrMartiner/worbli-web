import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { AlertService } from "../../../services";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { KycModalContent } from "./kyc-modal.component";

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    public config: Object = {};
    public profile: Object = null;
    public beginVerify: Boolean = false;
    public documentList: Array<Object> = [];

    constructor(
        private http: HttpClient,
        private modalService: NgbModal,
        private alertService: AlertService,
    ) { }

    ngOnInit() {
        this.loadHomeConfig();
        this.loadDocumentList();
        this.loadSelfProfile();
    }

    private loadHomeConfig(): void {
        this.http.get('/api/v1/config/dashboard')
            .subscribe((response) => {
                this.config = response['payload']
            }, (error) => {
                this.alertService.error('Error occurred at loading dashboard config.');
            });
    }

    private loadDocumentList(): void {
        this.http.get('/api/v1/document')
            .subscribe((response) => {
                this.documentList = response['payload']['results'];
            }, (error) => {
                this.alertService.error('Error occurred at loading documents.');
            });
    }

    private loadSelfProfile(): void {
        this.http.get('/api/v1/user/self')
            .subscribe((response) => {
                this.profile = response['payload'];
            }, (error) => {
                this.alertService.error('Error occurred at loading profile.');
            });
    }

    public openKycVerifyModal(): void {
        this.modalService.open(KycModalContent).result.then((result) => {
            setTimeout(this.loadSelfProfile, 120 * 3 + 200);
        }, (reason) => {
            setTimeout(this.loadSelfProfile, 120 * 3 + 200);
        });
    }
}
