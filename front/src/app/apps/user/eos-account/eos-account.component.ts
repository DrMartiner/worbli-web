import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AlertService} from "../../../services";
import {HttpClient} from "@angular/common/http";
import {UserService} from "../../../app.service";

@Component({
    selector: 'app-eos-account',
    templateUrl: './eos-account.component.html',
    styleUrls: ['./eos-account.component.scss']
})
export class EosAccountComponent implements OnInit {
    public form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private http: HttpClient,
        private alertService: AlertService,
        private userService: UserService
    ) {
        const profile = this.userService.getProfile();
        this.form = fb.group({
            eos_public_key : [null, Validators.required],
            eos_account : [null, Validators.required],
        });
    }

    ngOnInit() {
        this.loadUserProfile();
    }

    private loadUserProfile(): void {
        this.http.get('/api/v1/user/self/')
            .subscribe(
                (response) => this.form.patchValue(response['payload']),
                (error) => this.alertService.warn('Error occurred.')
            );
    }

    public submit(): void {
        this.http.patch('/api/v1/user/eos-account/', this.form.value)
            .subscribe(
                (response) => {
                    this.alertService.success('EOS account was updated.');
                }, (error) => this.alertService.warn('Error occurred.'));
    }
}
