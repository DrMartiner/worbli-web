import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AlertService} from "../../../services";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
    public form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private http: HttpClient,
        private alertService: AlertService,
    ) {
        this.form = fb.group({
            old_password : [null, Validators.required],
            new_password1 : [null, Validators.required],
            new_password2 : [null, Validators.required],
        });
    }

    ngOnInit() {}

    public submit(): void {
        this.http.post('/api/v1/auth/password/change/', this.form.value)
            .subscribe((response) => {
                this.alertService.success('Password was changed.');
            }, (error) => {
                switch(error['status']) {
                    case 400: {
                        this.alertService.warn('Bad data.');
                        break;
                    }
                    default: {
                        this.alertService.error('Error occurred.');
                        break;
                    }
                }
            });
    }
}
