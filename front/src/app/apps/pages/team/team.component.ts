import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { AlertService } from "../../../services";

@Component({
    selector: 'app-team',
    templateUrl: './team.component.html',
    styleUrls: ['./team.component.scss']
})
export class TeamComponent implements OnInit {
    public departamentList: Array<Object> = [];

    constructor(private http: HttpClient, private alertService: AlertService) { }

    ngOnInit() {
        this.loadStuffList();
    }

    private loadStuffList(): void {
        this.http.get('/api/v1/team/departament/')
            .subscribe((response) => {
                this.departamentList = response['payload']['results'];
            }, (error) => {
                this.alertService.error('Error occurred.');
            });
    }

    public openEmployeePopup(): void {
    }
}
