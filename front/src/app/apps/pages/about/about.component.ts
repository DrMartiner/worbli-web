import { Component, OnInit } from '@angular/core';
import {AlertService} from "../../../services";
import {HttpClient} from "@angular/common/http";

@Component({
    selector: 'app-about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {
    public aboutConfig: Object = {};

    constructor(
        private http: HttpClient,
        private alertService: AlertService,
    ) { }

    ngOnInit() {
        this.loadAboutConfig();
    }

    private loadAboutConfig(): void {
        this.http.get('/api/v1/config/about-us')
            .subscribe((response) => {
                this.aboutConfig = response['payload']
            }, (error) => {
                this.alertService.error('Error occurred.');
            });
    }
}
