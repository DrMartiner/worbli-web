import {Component, Input, OnInit} from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { AlertService } from "../../../services";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NotifyModalContent } from "../../shared/notify-modal.component";
import {IsAuthService} from "../../../app.service";

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
    public homeConfig: Object = {};
    public documentList: Array<Object> = [];
    public form: FormGroup;

    constructor(
        private fb: FormBuilder,
        private http: HttpClient,
        private alertService: AlertService,
        private modalService: NgbModal,
        public isAuthService: IsAuthService,
    ) {
        this.form = fb.group({
            email : [null, [Validators.required, Validators.email]],
        });
    }

    ngOnInit() {
        this.loadHomeConfig();
        this.loadDocumentList();
    }

    private loadHomeConfig(): void {
        this.http.get('/api/v1/config/home')
            .subscribe((response) => {
                this.homeConfig = response['payload']
            }, (error) => {
                this.alertService.error('Error occurred.');
            });
    }

    private loadDocumentList(): void {
        this.http.get('/api/v1/document')
            .subscribe((response) => {
                this.documentList = response['payload']['results'];
            }, (error) => {
                this.alertService.error('Error occurred.');
            });
    }

    public openSignUpPopup(): void {
        const modalRef = this.modalService.open(NotifyModalContent);
        modalRef.componentInstance.title = 'Join Now';

        this.http.post('/api/v1/auth/sign-up/', this.form.value)
            .subscribe((response) => {
                modalRef.componentInstance.text = `SignUp confirm code was sent to "${this.form.value['email']}".`;
                this.form.reset();
            }, (error) => {
                switch(error.status) {
                    case 410: {
                        modalRef.componentInstance.text = `Email "${this.form.value['email']}" is already exists.`;
                        break;
                    }
                    default: {
                        modalRef.componentInstance.text = 'Error occurred.';
                        break;
                    }
                }
            });
    }
}
