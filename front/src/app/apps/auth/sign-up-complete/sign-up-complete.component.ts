import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AlertService} from "../../../services";
import {HttpClient} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {LocalStorageService} from "ngx-store";
import {NotifyModalContent} from "../../shared/notify-modal.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
    selector: 'app-sign-up-complete',
    templateUrl: './sign-up-complete.component.html',
    styleUrls: ['./sign-up-complete.component.scss']
})
export class SignUpCompleteComponent implements OnInit {
    public form: FormGroup;
    public signUpCode: String = null;

    constructor(
        private fb: FormBuilder,
        private http: HttpClient,
        private alertService: AlertService,
        private route: ActivatedRoute,
        private router: Router,
        private localStorageService: LocalStorageService,
        private modalService: NgbModal
    ) {
        this.form = fb.group({
            first_name : [null, Validators.required],
            last_name : [null, Validators.required],
            // country : [null, Validators.required],
            password : [null, Validators.required],
            password_confirm : [null, Validators.required],
            have_eos_account : [null],
            is_agree : [null, Validators.required],
        });
    }

    ngOnInit() {
        this.checkCode();
    }

    public checkCode(): void {
        this.route.queryParams.subscribe(params => {
            this.signUpCode = this.route.snapshot.queryParams['sign_up_code'];

            this.http.post('/api/v1/auth/sign-up/check-code/', {sign_up_code: this.signUpCode})
                .subscribe((response) => {}, (error) => this.checkResponseErrorCode(error));
        });
    }

    public submit(): void {
        let postData = this.form.value;
        postData['sign_up_code'] = this.signUpCode;

        this.http.post('/api/v1/auth/sign-up/complete/', postData)
            .subscribe(
                (response) => {
                    this.localStorageService.set('isAuth', true);

                    this.alertService.success('SignUp was completed.');

                    if (this.form.value['have_eos_account']) {
                        this.router.navigate(['/user/eos-account']);
                    }
                    else {
                        const modalRef = this.modalService.open(NotifyModalContent);
                        modalRef.componentInstance.title = 'Join Now';
                        modalRef.componentInstance.html = `
                            <div class="modal-worbli-title">How to create an EOS account?</div>
                            <ol>
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                    labore et dolore magna aliqua.
                                </li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                    labore et dolore magna aliqua.
                                </li>
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                    labore et dolore magna aliqua.
                                </li>
                            </ol>
                        `;

                        setTimeout(()=> this.router.navigate(['/']),3000);
                    }

                }, (error) => this.checkResponseErrorCode(error));
    }

    private checkResponseErrorCode(error: Object): void {
        switch(error['status']) {
            case 400: {
                this.alertService.warn('Bad data.');
                break;
            }
            case 404: {
                this.alertService.warn('SignUp code was not found.');
                setTimeout(()=> this.router.navigate(['/not-found']),3000);
                break;
            }
            case 409: {
                this.alertService.warn('SignUp code was already activated.');
                setTimeout(()=> this.router.navigate(['/']),3000);
                break;
            }
            case 410: {
                this.alertService.warn('SignUp code is expired.');
                setTimeout(()=> this.router.navigate(['/']),3000);
                break;
            }
            default: {
                this.alertService.error('Error occurred.');
                break;
            }
        }
    }
}
