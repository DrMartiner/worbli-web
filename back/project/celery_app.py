import os

import celery
from django.apps import apps

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings')

app = celery.Celery('worbli')
app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: [cfg.name for cfg in apps.get_app_configs()])
