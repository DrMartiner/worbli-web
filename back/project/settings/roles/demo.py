AUTH_PASSWORD_VALIDATORS = []
PASSWORD_HASHERS = ['django.contrib.auth.hashers.MD5PasswordHasher',]

DJANGO_TWILIO_FORGERY_PROTECTION = True

DATA_UPLOAD_MAX_MEMORY_SIZE = 2621440 * 5

BASE_URL = 'http://localhost:8080'
