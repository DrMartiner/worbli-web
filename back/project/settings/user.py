from .env import env

AUTH_USER_MODEL = 'users.User'

AUTHENTEQ = {
    'PARTNER_ID': env('AUTHENTEQ_PARTNER_ID'),
    'API_KEY': env('AUTHENTEQ_API_KEY'),
    'API_ROOT': env('AUTHENTEQ_API_ROOT'),
}
