import os

import environ

root = environ.Path(__file__, '../../..')
env = environ.Env(
    ROLE=(str, None),
    DEBUG=(bool, False),
    ALLOWED_HOSTS=(list, ['*']),

    SECRET_KEY=(str, 'r%&^%#bguhp[0-897tr85e47druftgyuh!Ayocv6uk'),
    BROKER_URL=(str, 'redis://redis:6379'),
    CELERY_BROKER_URL=(str, 'redis://redis:6379'),
    CELERY_RESULT_BACKEND_URL=(str, 'redis://redis:6379'),

    EMAIL_HOST_USER=(str, ''),
    EMAIL_HOST_PASSWORD=(str, ''),

    BASE_URL=(str, ''),

    AUTHENTEQ_PARTNER_ID=(str, None),
    AUTHENTEQ_API_KEY=(str, None),
    AUTHENTEQ_API_ROOT=(str, 'https://api.authenteq.com'),
)

# Load env.ini
env_file_path = root('.env.ini')
if not os.path.exists(env_file_path):
    env_file_path = root('env.ini')
env.read_env(env_file_path)
