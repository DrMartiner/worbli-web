from .env import env
from . import common

BROKER_URL = env('BROKER_URL')
CELERY_BROKER_URL = env('CELERY_BROKER_URL')
CELERY_RESULT_BACKEND = env('CELERY_RESULT_BACKEND_URL')
CELERY_ACCEPT_CONTENT = ['application/x-python-serialize']  # ['application/json']
CELERY_TASK_SERIALIZER = 'pickle'  # json
CELERY_RESULT_SERIALIZER = 'json'

CELERY_TIMEZONE = common.TIME_ZONE
CELERY_ENABLE_UTC = True

CELERY_EMAIL_TASK_CONFIG = {
    'queue': 'email',
    'rate_limit': '50/m',
}
