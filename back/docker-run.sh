#!/bin/sh

pipenv install --deploy --ignore-pipfile --system

./manage.py migrate
./manage.py loaddata project/fixtures/$ROLE/*
./manage.py collectstatic --noinput

rm /tmp/celery*
export C_FORCE_ROOT='true'  # for run Celery
celery -A project worker -E -Q email \
        --beat \
        --loglevel=info \
        --pidfile="/tmp/celery.pid" \
        --schedule="/tmp/celerybeat-schedule" &

gunicorn project.wsgi:application -w 2 --log-level=info --bind=back:8000 --reload
