from rest_framework.response import Response

from rest_framework.views import APIView

from apps.config import models
from . import serializers


class HomeConfigView(APIView):
    permission_classes = []

    def get(self, request, *args, **kwargs):
        instance = models.HomeConfig.get_solo()
        serializer = serializers.HomeConfigSerializer(instance=instance)

        return Response(serializer.data)


class AboutUsConfigView(APIView):
    permission_classes = []

    def get(self, request, *args, **kwargs):
        instance = models.AboutUsConfig.get_solo()
        serializer = serializers.AboutUsConfigSerializer(instance=instance)

        return Response(serializer.data)


class DashboardConfigView(APIView):
    permission_classes = []

    def get(self, request, *args, **kwargs):
        instance = models.DashboardConfig.get_solo()
        serializer = serializers.DashboardConfigSerializer(instance=instance)

        return Response(serializer.data)
