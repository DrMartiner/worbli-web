from django.urls import path

from . import views


urlpatterns = [
    path('home/', views.HomeConfigView.as_view(), name='home-config'),
    path('about-us/', views.AboutUsConfigView.as_view(), name='about-us-config'),
    path('dashboard/', views.DashboardConfigView.as_view(), name='dashboard-config'),
]
