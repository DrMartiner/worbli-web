from django_dynamic_fixture import G
from rest_framework.reverse import reverse

from apps.config.models import HomeConfig
from ...base_test import BaseApiTest
from .. import serializers


class HomeConfigTest(BaseApiTest):
    def test_read_detail(self):
        config: HomeConfig = G(HomeConfig)

        url = reverse('api_v1:config:home-config')
        response = self.client.get(url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(serializers.HomeConfigSerializer, config, response)
