from django_dynamic_fixture import G
from rest_framework.reverse import reverse

from apps.config.models import DashboardConfig
from ...base_test import BaseApiTest
from .. import serializers


class DashboardConfigTest(BaseApiTest):
    def test_read_detail(self):
        config: DashboardConfig = G(DashboardConfig)

        url = reverse('api_v1:config:dashboard-config')
        response = self.client.get(url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(serializers.DashboardConfigSerializer, config, response)
