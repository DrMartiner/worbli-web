from rest_framework import serializers

from apps.config import models


class HomeConfigSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.HomeConfig
        fields = '__all__'


class AboutUsConfigSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.AboutUsConfig
        fields = '__all__'


class DashboardConfigSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.DashboardConfig
        fields = '__all__'
