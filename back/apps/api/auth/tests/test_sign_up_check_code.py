from rest_framework.reverse import reverse

from ...base_test import BaseApiTest


class SignUpTest(BaseApiTest):
    url = reverse('api_v1:auth:sign-up-check-code')

    def setUp(self):
        super(SignUpTest, self).setUp()

        self.post_data = {'email': 'new@mail.ru'}

    def test_right_code(self):
        self.update_user_instance(is_active=False)

        post_data = {'sign_up_code': self.user.sign_up_code}
        response = self.client.post(self.url, data=post_data)
        self.assertResponseStatus200(response)

    def test_wrong_code(self):
        self.update_user_instance(is_active=False)

        post_data = {'sign_up_code': 'wrong'}
        response = self.client.post(self.url, data=post_data)
        self.assertResponseStatus404(response)

    def test_was_activated(self):
        post_data = {'sign_up_code': self.user.sign_up_code}
        response = self.client.post(self.url, data=post_data)
        self.assertResponseStatus409(response)
