from rest_framework.reverse import reverse

from apps.api.user.serializers import UserSelfReadSerializer
from ...base_test import BaseApiTest


class SignInTest(BaseApiTest):
    url = reverse('api_v1:auth:sign-in')

    def setUp(self):
        super(SignInTest, self).setUp()

        self.post_data = {'email': self.user.email, 'password': self.password}

    def test_sign_in(self):
        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus200(response)

        self.assertIn('sessionid', response.data)
        self.assertSessionid(response.data['sessionid'], self.user)

        response_data = response.data.copy()
        del response_data['sessionid']
        serializer = UserSelfReadSerializer(instance=self.user)
        self.assertDictEqual(response_data, serializer.data)

    def test_not_active(self):
        self.update_user_instance(is_active=False)

        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus406(response)

        self.assertNotIn('sessionid', response.data)

    def test_wrong_password(self):
        self.post_data.update({'password': 'wrong'})

        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus401(response)

        self.assertNotIn('sessionid', response.data)

    def test_wrong_email(self):
        self.post_data.update({'email': 'wrong@mail.ru'})

        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus404(response)

        self.assertNotIn('sessionid', response.data)