from django.core import mail
from rest_framework.reverse import reverse

from ...base_test import BaseApiTest


class PasswordRestoreTest(BaseApiTest):
    url = reverse('api_v1:auth:password-restore')

    def setUp(self):
        super(PasswordRestoreTest, self).setUp()

        self.post_data = {'email': self.user.email}

    def test_is_activated(self):
        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus202(response)

        self.assertEqual(len(mail.outbox), 1)
        self.assertListEqual(mail.outbox[0].to, [self.user.email])

        old_restore_password_code = self.user.restore_password_code
        old_restore_password_created = self.user.restore_password_created

        self.reload_user_instance()

        self.assertNotEquals(self.user.restore_password_code, old_restore_password_code)
        self.assertNotEquals(self.user.restore_password_created, old_restore_password_created)

        self.assertIn(self.user.restore_password_code, mail.outbox[0].body)

    def test_is_not_activated(self):
        self.update_user_instance(is_active=False)

        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus406(response)

        self.assertEqual(len(mail.outbox), 1)
        self.assertListEqual(mail.outbox[0].to, [self.user.email])

        old_restore_password_code = self.user.restore_password_code
        old_restore_password_created = self.user.restore_password_created

        self.reload_user_instance()

        self.assertNotEquals(self.user.restore_password_code, old_restore_password_code)
        self.assertNotEquals(self.user.restore_password_created, old_restore_password_created)

        self.assertIn(self.user.restore_password_code, mail.outbox[0].body)

    def test_wrong_email(self):
        self.post_data['email'] = 'wrong@mail.ru'

        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus404(response)

        self.assertEqual(len(mail.outbox), 0)
