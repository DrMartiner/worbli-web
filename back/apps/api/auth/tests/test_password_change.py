from rest_framework.reverse import reverse

from ...base_test import BaseApiTest


class PasswordChangeTest(BaseApiTest):
    url = reverse('api_v1:auth:password-change')

    new_password: str = 'new-Password-123'

    def setUp(self):
        super(PasswordChangeTest, self).setUp()

        self.post_data = {
            'old_password': self.password,
            'new_password1': self.new_password,
            'new_password2': self.new_password,
        }

    def test_change(self):
        self.client.force_login(self.user)
        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus202(response)

        self.reload_user_instance()

        is_right = self.user.check_password(self.new_password)
        self.assertTrue(is_right)

    def test_un_auth(self):
        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus403(response)

