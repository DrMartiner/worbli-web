from rest_framework.reverse import reverse

from apps.api.user.serializers import UserSelfReadSerializer
from ...base_test import BaseApiTest


class SignUpCompleteTest(BaseApiTest):
    url = reverse('api_v1:auth:sign-up-complete')

    new_password = 'new-sec-password123'

    def setUp(self):
        super(SignUpCompleteTest, self).setUp()

        self.post_data = {
            'sign_up_code': self.user.sign_up_code,
            'password': self.new_password,
            'password_confirm': self.new_password,
            'is_agree': True,
            'country': 'RU',
        }

        self.update_user_instance(is_active=False)

    def test_complete(self):
        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus202(response)

        self.reload_user_instance()

        self.assertTrue(self.user.is_active)

        # TODO: fix it
        # country = Country(code=self.post_data['country'])
        # self.assertIsNotNone(self.user.country)
        # self.assertEquals(self.user.country.code, country.code)

        is_right = self.user.check_password(self.new_password)
        self.assertTrue(is_right)

        response_data = response.data.copy()
        del response_data['sessionid']
        serializer = UserSelfReadSerializer(instance=self.user)
        self.assertDictEqual(response_data, serializer.data)

    def test_wrong_password(self):
        self.post_data['password'] = 'wrong'

        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus400(response)

        self.reload_user_instance()

        self.assertFalse(self.user.is_active)

        is_right = self.user.check_password(self.new_password)
        self.assertFalse(is_right)

    def test_wrong_password_confirm(self):
        self.post_data['password_confirm'] = 'wrong'

        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus400(response)

        self.reload_user_instance()

        self.assertFalse(self.user.is_active)

        is_right = self.user.check_password(self.new_password)
        self.assertFalse(is_right)

    def test_wrong_sign_up_code(self):
        self.post_data['sign_up_code'] = 'wrong'

        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus404(response)

        self.reload_user_instance()

        self.assertFalse(self.user.is_active)

        is_right = self.user.check_password(self.new_password)
        self.assertFalse(is_right)
