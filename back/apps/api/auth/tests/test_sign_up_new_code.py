from django.core import mail
from rest_framework.reverse import reverse

from ...base_test import BaseApiTest


class SignUpNewCodeTest(BaseApiTest):
    url = reverse('api_v1:auth:sign-up-new-code')

    def test_is_not_activated(self):
        self.update_user_instance(is_active=False)

        response = self.client.post(self.url, data={'email': self.user.email})
        self.assertResponseStatus202(response)

        self.assertEqual(len(mail.outbox), 1)
        self.assertListEqual(mail.outbox[0].to, [self.user.email])

        old_sign_up_code = self.user.sign_up_code
        old_sign_up_created = self.user.sign_up_code_created

        self.reload_user_instance()

        self.assertNotEquals(self.user.sign_up_code, old_sign_up_code)
        self.assertNotEquals(self.user.sign_up_code_created, old_sign_up_created)

        self.assertIn(self.user.sign_up_code, mail.outbox[0].body)

    def test_is_activated(self):
        response = self.client.post(self.url, data={'email': self.user.email})
        self.assertResponseStatus410(response)

        self.assertEqual(len(mail.outbox), 0)

        old_sign_up_code = self.user.sign_up_code
        old_sign_up_created = self.user.sign_up_code_created

        self.reload_user_instance()

        self.assertEquals(self.user.sign_up_code, old_sign_up_code)
        self.assertEquals(self.user.sign_up_code_created, old_sign_up_created)

    def test_wrong_email(self):
        response = self.client.post(self.url, data={'email': 'wrong@mail.ru'})
        self.assertResponseStatus404(response)

        self.assertEqual(len(mail.outbox), 0)

