from datetime import timedelta

from django.core import mail
from django.utils import timezone
from rest_framework.reverse import reverse

from ...base_test import BaseApiTest


class PasswordRestoreCompleteTest(BaseApiTest):
    url = reverse('api_v1:auth:password-restore-complete')

    def setUp(self):
        super(PasswordRestoreCompleteTest, self).setUp()

        self.post_data = {'restore_password_code': self.user.restore_password_code}

    def test_is_activated(self):
        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus202(response)

        self.assertEqual(len(mail.outbox), 1)
        self.assertListEqual(mail.outbox[0].to, [self.user.email])

        self.reload_user_instance()

        is_right = self.user.check_password(self.password)
        self.assertFalse(is_right)

    def test_is_not_activated(self):
        self.update_user_instance(is_active=False)

        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus202(response)

        self.assertEqual(len(mail.outbox), 1)
        self.assertListEqual(mail.outbox[0].to, [self.user.email])

        self.reload_user_instance()
        self.assertTrue(self.user.is_active)

        is_right = self.user.check_password(self.password)
        self.assertFalse(is_right)

    def test_wrong_restore_password_code(self):
        post_data = {'restore_password_code': 'wrong'}
        response = self.client.post(self.url, data=post_data)
        self.assertResponseStatus404(response)

        self.assertEqual(len(mail.outbox), 0)

        is_right = self.user.check_password(self.password)
        self.assertTrue(is_right)

    def test_is_expired(self):
        self.update_user_instance(
            is_active=False,
            restore_password_created=timezone.now() - timedelta(days=100)
        )

        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus410(response)

        old_restore_password_code = self.user.restore_password_code
        old_restore_password_created = self.user.restore_password_created

        self.reload_user_instance()

        self.assertNotEqual(self.user.restore_password_code, old_restore_password_code)
        self.assertNotEqual(self.user.restore_password_created, old_restore_password_created)

        self.assertEqual(len(mail.outbox), 1)
        self.assertListEqual(mail.outbox[0].to, [self.user.email])
        self.assertIn(self.user.restore_password_code, mail.outbox[0].body)

        self.assertTrue(self.user.is_active)

        is_right = self.user.check_password(self.password)
        self.assertTrue(is_right)
