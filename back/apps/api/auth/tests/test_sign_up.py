from datetime import timedelta

from django.core import mail
from django.utils import timezone
from rest_framework.reverse import reverse

from apps.common.utils import get_object_or_none
from apps.users.models import User
from ...base_test import BaseApiTest


class SignUpTest(BaseApiTest):
    url = reverse('api_v1:auth:sign-up')

    def setUp(self):
        super(SignUpTest, self).setUp()

        self.post_data = {'email': 'new@mail.ru'}

    def test_sign_up(self):
        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus201(response)

        user: User = get_object_or_none(User, email=self.post_data['email'])
        self.assertIsNotNone(user)
        self.assertFalse(user.is_active)

        self.assertEqual(len(mail.outbox), 1)
        self.assertListEqual(mail.outbox[0].to, [self.post_data['email']])

        self.assertIn(user.sign_up_code, mail.outbox[0].body)

    def test_is_active(self):
        response = self.client.post(self.url, data={'email': self.email})
        self.assertResponseStatus410(response)

        count = User.objects.filter(email=self.email).count()
        self.assertEqual(count, 1)

        self.assertEqual(len(mail.outbox), 0)

    def test_not_is_active_expired(self):
        sign_up_code = '123'
        sign_up_code_created = timezone.now() - timedelta(days=100500)
        self.update_user_instance(
            is_active=False,
            sign_up_code=sign_up_code,
            sign_up_code_created=sign_up_code_created,
        )

        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus201(response)

        user: User = get_object_or_none(User, email=self.post_data['email'])
        self.assertIsNotNone(user)
        self.assertFalse(user.is_active)
        self.assertNotEquals(user.sign_up_code, sign_up_code)
        self.assertNotEquals(user.sign_up_code_created, sign_up_code_created)

        self.assertEqual(len(mail.outbox), 1)
        self.assertListEqual(mail.outbox[0].to, [self.post_data['email']])

        self.assertIn(user.sign_up_code, mail.outbox[0].body)
