from django.conf import settings
from django.contrib.auth import login, logout
from rest_framework import status, permissions
from rest_framework.response import Response
from django.utils.translation import gettext_lazy as _
from templated_email import send_templated_mail

from apps.api.user.serializers import UserSelfReadSerializer
from apps.api.utils import get_session, get_user_by_session

from rest_framework.views import APIView

from apps.common.utils import get_object_or_none
from apps.users.models import User, get_random_str
from . import serializers


class SignUpView(APIView):
    permission_classes = []
    messages_map = {
        201: _('User was created and Sign-Up code was sent.'),
        202: _('Sign-Up code was sent.'),
        410: _('User was already activated.'),
    }

    def post(self, request, *args, **kwargs):
        serializer = serializers.SignUpSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user: User = get_object_or_none(User, email=serializer.data['email'])
        if user:
            if user.is_active:
                return Response({}, status=status.HTTP_410_GONE)
            else:
                user.update_sign_up_code()
                user.send_sign_up_code(request)

                return Response({}, status=status.HTTP_202_ACCEPTED)
        else:
            user = User.objects.create(
                email=serializer.data['email'],
                username=serializer.data['email'],
                is_active=False
            )
            user.send_sign_up_code(request)

            return Response({}, status=status.HTTP_201_CREATED)


class SignUpCheckCodeSerializer(APIView):
    permission_classes = []
    messages_map = {
        200: _('Sign-Up code was sent.'),
        404: _('Sign-Up code was not found.'),
        409: _('User was already activated.'),
        410: _('SignUp code is expired.'),
    }

    def post(self, request, *args, **kwargs):
        serializer = serializers.SignUpCheckCodeSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user: User = get_object_or_none(User, sign_up_code=serializer.data['sign_up_code'])
        if user:
            if user.is_active:
                return Response({}, status=status.HTTP_409_CONFLICT)

            if user.is_sign_up_code_expired:
                return Response({}, status=status.HTTP_410_GONE)
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        return Response({}, status=status.HTTP_200_OK)


class SignUpNewCodeSerializer(APIView):
    permission_classes = []
    messages_map = {
        202: _('Sign-Up code was sent.'),
        404: _('Email was not found.'),
        410: _('User was already activated.'),
    }

    def post(self, request, *args, **kwargs):
        serializer = serializers.SignUpNewCodeSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user: User = get_object_or_none(User, email=serializer.data['email'])
        if user:
            if user.is_active:
                return Response({}, status=status.HTTP_410_GONE)
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        user.update_sign_up_code()
        user.send_sign_up_code(request)

        return Response({}, status=status.HTTP_202_ACCEPTED)


class SignUpCompleteView(APIView):
    permission_classes = []
    messages_map = {
        202: _('Sign-Up was completed.'),
        404: _('Sign-Up code was not found.'),
        409: _('User was already activated.'),
        410: _('SignUp code is expired.'),
    }

    def post(self, request, *args, **kwargs):
        serializer = serializers.SignUpCompleteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user: User = get_object_or_none(User, sign_up_code=serializer.data['sign_up_code'])
        if user:
            if user.is_active:
                return Response({}, status=status.HTTP_409_CONFLICT)

            if user.is_sign_up_code_expired:
                return Response({}, status=status.HTTP_410_GONE)
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        user.is_active = True
        user.set_password(serializer.data['password'])
        user.save()

        login(request, user, 'django.contrib.auth.backends.ModelBackend')

        session = get_session(user)

        serializer = UserSelfReadSerializer(instance=user)
        data = serializer.data.copy()
        data.update({'sessionid': session.session_key})

        return Response(data, status=status.HTTP_202_ACCEPTED)


class SignInView(APIView):
    permission_classes = []
    messages_map = {
        404: _('Email was not found.'),
        406: _('User was not activated.'),
        401: _('Password is wrong.'),
        410: _('User was already activated.'),
    }

    def post(self, request, *args, **kwargs):
        serializer = serializers.SignInSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user: User = get_object_or_none(User, email=serializer.data['email'])
        if user:
            if user.is_active:
                is_right = user.check_password(serializer.data['password'])
                if not is_right:
                    return Response({}, status=status.HTTP_401_UNAUTHORIZED)
            else:
                return Response({}, status=status.HTTP_406_NOT_ACCEPTABLE)
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        login(request, user, 'django.contrib.auth.backends.ModelBackend')

        session = get_session(user)

        serializer = UserSelfReadSerializer(instance=user)
        data = serializer.data.copy()
        data.update({'sessionid': session.session_key})

        return Response(data, status=status.HTTP_200_OK)


class PasswordChangeView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    messages_map = {
        202: _('Password was changed.'),
        401: _('Password is wrong.'),
    }

    def post(self, request, *args, **kwargs):
        serializer = serializers.PasswordChangeSerializer(
            data=request.data,
            context={'request': request}
        )
        serializer.is_valid(raise_exception=True)

        request.user.set_password(serializer.data['new_password1'])
        request.user.save()

        return Response({}, status=status.HTTP_202_ACCEPTED)


class PasswordRestoreView(APIView):
    permission_classes = []
    messages_map = {
        404: _('Email was not found.'),
        406: _('User was not activated.'),
    }

    def post(self, request, *args, **kwargs):
        serializer = serializers.RestorePasswordSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user: User = get_object_or_none(User, email=serializer.data['email'])
        if user:
            user.update_restore_password_code()
            user.send_restore_code(request)

            if user.is_active:
                return Response({}, status=status.HTTP_202_ACCEPTED)
            else:
                return Response({}, status=status.HTTP_406_NOT_ACCEPTABLE)
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)


class PasswordRestoreCompleteView(APIView):
    permission_classes = []
    messages_map = {
        202: _('Restore password code was not found.'),
        404: _('Restore password code was not found.'),
        406: _('User was not activated.'),
        410: _('Restore password code is expired.'),
    }

    def post(self, request, *args, **kwargs):
        serializer = serializers.RestorePasswordCompleteSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        user: User = get_object_or_none(User, restore_password_code=serializer.data['restore_password_code'])
        if user:
            if not user.is_active:
                user.is_active = True
                user.save()

            if user.is_restore_password_code_expired:
                user.update_restore_password_code()
                user.send_restore_code(request)

                return Response({}, status=status.HTTP_410_GONE)
        else:
            return Response({}, status=status.HTTP_404_NOT_FOUND)

        password = get_random_str()[:10]
        user.set_password(password)
        user.save()

        send_templated_mail(
            template_name='auth_restore_password_complete',
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[user.email],
            context={'request': request, 'user': self, 'password': password}
        )

        return Response({}, status=status.HTTP_202_ACCEPTED)


class CheckSessionidView(APIView):
    permission_classes = []
    authentication_classes = []

    messages_map = {
        404: _('SessionID was not found.'),
    }

    def post(self, request, *args, **kwargs):
        if 'sessionid' in request.data:
            data = request.data
        else:
            data = {'sessionid': request.COOKIES.get('sessionid')}

        serializer = serializers.SignUpSessionSerializer(data=data)
        serializer.is_valid(raise_exception=True)

        user = get_user_by_session(serializer.data['sessionid'])

        data = {'is_active': user.is_active}
        return Response(data, status=status.HTTP_200_OK)


class SignOutView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    messages_map = {
        202: _('SignOut was succeed.'),
    }

    def post(self, request, *args, **kwargs):
        logout(request)

        return Response({}, status=status.HTTP_202_ACCEPTED)
