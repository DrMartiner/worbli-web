from django_countries.serializer_fields import CountryField
from rest_framework import serializers
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import AuthenticationFailed, ValidationError

from apps.api.exceptions import ConflictError
from apps.users.models import User


class SignUpSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)


class SignUpCheckCodeSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['sign_up_code']


class SignUpNewCodeSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)


class SignUpCompleteSerializer(serializers.ModelSerializer):
    sign_up_code = serializers.CharField(required=True)

    password = serializers.CharField(required=True)
    password_confirm = serializers.CharField(required=True)

    is_agree = serializers.BooleanField(required=True)

    country = CountryField(required=False)

    def validate_password_confirm(self, password_confirm: str) -> str:
        if password_confirm != self.initial_data.get('password'):
            raise ValidationError(_('Passwords are not equal.'))

        return password_confirm

    def validate_is_agree(self, is_agree: bool) -> bool:
        if not is_agree:
            raise ValidationError(_('You should be agree.'))

        return is_agree

    class Meta:
        model = User
        fields = ['sign_up_code', 'first_name', 'last_name', 'country', 'password', 'password_confirm', 'is_agree']


class SignInSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)
    password = serializers.CharField(required=True)


class PasswordChangeSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    new_password1 = serializers.CharField(required=True)
    new_password2 = serializers.CharField(required=True)

    def __init__(self, context: dict, *args, **kwargs):
        self.user = context['request'].user

        super(PasswordChangeSerializer, self).__init__(*args, **kwargs)

    def validate_old_password(self, old_password: str) -> str:
        if not self.user.check_password(old_password):
            raise AuthenticationFailed(_('Old password is wrong.'))

        return old_password

    def validate_new_password2(self, new_password2: str) -> str:
        if self.initial_data.get('new_password1') != new_password2:
            message = _('New passwords are not equal.')
            raise ConflictError(message)

        return new_password2


class RestorePasswordSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True)


class RestorePasswordCompleteSerializer(serializers.Serializer):
    restore_password_code = serializers.CharField(required=True)


class SignUpSessionSerializer(serializers.Serializer):
    sessionid = serializers.CharField(required=True)
