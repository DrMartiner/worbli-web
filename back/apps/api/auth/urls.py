from django.urls import path

from . import views

urlpatterns = [
    path('sign-up/', views.SignUpView.as_view(), name='sign-up'),
    path('sign-up/check-code/', views.SignUpCheckCodeSerializer.as_view(), name='sign-up-check-code'),
    path('sign-up/new-code/', views.SignUpNewCodeSerializer.as_view(), name='sign-up-new-code'),
    path('sign-up/complete/', views.SignUpCompleteView.as_view(), name='sign-up-complete'),

    path('check-sessionid/', views.CheckSessionidView.as_view(), name='check-sessionid'),

    path('sign-in/', views.SignInView.as_view(), name='sign-in'),
    path('sign-out/', views.SignOutView.as_view(), name='sign-out'),

    path('password/change/', views.PasswordChangeView.as_view(), name='password-change'),

    path('password/restore/', views.PasswordRestoreView.as_view(), name='password-restore'),
    path('password/restore/complete/', views.PasswordRestoreCompleteView.as_view(), name='password-restore-complete'),
]
