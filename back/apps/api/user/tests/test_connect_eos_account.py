from rest_framework.reverse import reverse

from apps.api.user.serializers import ConnectEosAccountSerializer
from ...base_test import BaseApiTest


class ConnectEosAccountTest(BaseApiTest):
    url = reverse('api_v1:user:self-eos-account')

    def setUp(self):
        super(ConnectEosAccountTest, self).setUp()

        self.post_data = {
            'eos_public_key': 'eos-public-key',
            'eos_account': 'eos-account',
        }

    def test_update(self):
        self.client.force_login(self.user)

        response = self.client.patch(self.url, data=self.post_data)
        self.assertResponseStatus200(response)

        self.reload_user_instance()
        self.assertRetrieveInResponse(ConnectEosAccountSerializer, self.user, response, self.user)

    def test_un_auth(self):
        response = self.client.patch(self.url, data=self.post_data)
        self.assertResponseStatus403(response)
