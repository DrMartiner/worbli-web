from rest_framework.reverse import reverse

from apps.api.user.serializers import UserSelfReadSerializer
from apps.common.utils import get_object_or_none
from apps.users.models import User
from ...base_test import BaseApiTest


class UserSelfTest(BaseApiTest):
    url = reverse('api_v1:user:self')

    def setUp(self):
        super(UserSelfTest, self).setUp()

        self.post_data = {
            'first_name': 'John',
            'last_name': 'Smitt',
            'country': 'NL',
        }

    def test_read(self):
        self.client.force_login(self.user)

        response = self.client.get(self.url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(UserSelfReadSerializer, self.user, response, self.user)

    def test_read_un_auth(self):
        response = self.client.get(self.url)
        self.assertResponseStatus403(response)

    def test_update(self):
        self.client.force_login(self.user)

        response = self.client.patch(self.url, self.post_data)
        self.assertResponseStatus200(response)

        user: User = get_object_or_none(User, **self.post_data)
        self.assertIsNotNone(user)

    def test_update_un_auth(self):
        response = self.client.patch(self.url, self.post_data)
        self.assertResponseStatus403(response)

        user: User = get_object_or_none(User, **self.post_data)
        self.assertIsNone(user)

    def test_delete(self):
        self.client.force_login(self.user)

        response = self.client.delete(self.url)
        self.assertResponseStatus200(response)

        user: User = get_object_or_none(User, pk=self.user.pk)
        self.assertIsNone(user)

    def test_delete_un_auth(self):
        response = self.client.delete(self.url)
        self.assertResponseStatus403(response)

        user: User = get_object_or_none(User, pk=self.user.pk)
        self.assertIsNotNone(user)
