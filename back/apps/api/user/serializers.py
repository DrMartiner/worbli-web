from django_countries.serializer_fields import CountryField
from rest_framework import serializers

from apps.users.models import User


class UserSelfReadSerializer(serializers.ModelSerializer):
    country = CountryField(country_dict=True)
    is_verify = serializers.NullBooleanField(read_only=True)

    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name', 'country', 'is_verify']


class UserSelfUpdateSerializer(serializers.ModelSerializer):
    country = CountryField()

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'country']


class ConnectEosAccountSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['eos_public_key', 'eos_account']
