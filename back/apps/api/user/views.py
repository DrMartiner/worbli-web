from rest_framework import permissions
from rest_framework.response import Response

from rest_framework.views import APIView

from apps.users.models import User
from . import serializers


class UserSelfView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, *args, **kwargs):
        serializer = serializers.UserSelfReadSerializer(instance=request.user)

        return Response(serializer.data)

    def patch(self, request, *args, **kwargs):
        serializer = serializers.UserSelfUpdateSerializer(
            data=request.data,
            instance=request.user
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        instance = User.objects.get(pk=request.user.pk)
        serializer = serializers.UserSelfReadSerializer(instance=instance)

        return Response(serializer.data)

    def delete(self, request, *args, **kwargs):
        request.user.delete()

        return Response({})


class ConnectEosAccountView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    def patch(self, request, *args, **kwargs):
        serializer = serializers.ConnectEosAccountSerializer(
            data=request.data,
            instance=request.user
        )
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data)
