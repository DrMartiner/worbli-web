from django.urls import path

from . import views


urlpatterns = [
    path('self/', views.UserSelfView.as_view(), name='self'),
    path('eos-account/', views.ConnectEosAccountView.as_view(), name='self-eos-account'),
]
