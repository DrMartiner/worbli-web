import logging

import requests
from celery import shared_task
from django.conf import settings
from templated_email import send_templated_mail

from apps.users.models import User

logger = logging.getLogger('django.request')


@shared_task(queue='kyc')
def check_kyc_data(user_id: int, **kwargs):
    try:
        user = User.objects.get(pk=user_id)
    except User.DoesNotExist:
        return logger.error(f'UserID={user_id} was not found')

    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
    }

    payload = {
        'partnerId': settings.AUTHENTEQ['PARTNER_ID'],
        'apiKey': settings.AUTHENTEQ['API_KEY'],
        'userToken': kwargs.pop('usertoken'),
        'value': None,
    }

    for key in kwargs:
        url = f"{settings.AUTHENTEQ['API_ROOT']}/api/v1/claims/{key}"

        data = payload.copy()
        data['value'] = kwargs[key]

        response = requests.post(url, json=data, headers=headers)

        messages = {}

        if response.ok:
            response_data = response.json()
            if response_data.get('claim'):
                setattr(user, f'is_{key}_verify', True)
                user.save()
            else:
                setattr(user, f'is_{key}_verify', False)
                messages[key] = response_data.get('message')
        else:
            messages[key] = 'Verify is fail'

        send_templated_mail(
            template_name='kyc_result',
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[user.email],
            context={'messages': messages, 'user': user}
        )
