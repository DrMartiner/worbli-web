from django.conf.urls import url
from django.urls import path, include
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework.permissions import AllowAny

schema_view = get_schema_view(
  openapi.Info(
    title="Worbli API",
    default_version='v1',
    description='Worbli API',
    terms_of_service="https://www.google.com/policies/terms/",
    contact=openapi.Contact(email='DrMartiner@GMail.Com'),
    license=openapi.License(name='BSD License'),
  ),
  validators=['flex', 'ssv'],
  public=True,
  permission_classes=(AllowAny, ),
)

urlpatterns = [
  path('auth/', include(('apps.api.auth.urls', 'apps.api'), namespace='auth')),
  path('config/', include(('apps.api.config.urls', 'apps.api'), namespace='config')),
  path('document/', include(('apps.api.document.urls', 'apps.api'), namespace='document')),
  path('team/', include(('apps.api.team.urls', 'apps.api'), namespace='team')),
  path('user/', include(('apps.api.user.urls', 'apps.api'), namespace='user')),
  path('kyc/', include(('apps.api.kyc.urls', 'apps.api'), namespace='kyc')),

  url('^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
  path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
  path('swagger/redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
