from rest_framework import serializers


class CheckingSerializer(serializers.Serializer):
    usertoken = serializers.CharField(required=True)
    givenname = serializers.CharField(required=True)
    surname = serializers.CharField(required=True)
    passportno = serializers.CharField(required=True)
