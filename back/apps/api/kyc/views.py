from rest_framework import permissions, status
from django.utils.translation import gettext_lazy as _
from rest_framework.response import Response

from rest_framework.views import APIView

from apps.api.tasks import check_kyc_data
from . import serializers


class CheckingView(APIView):
    permission_classes = [permissions.IsAuthenticated]
    messages_map = {
        202: _('User data was accepted to checking.'),
    }

    def post(self, request, *args, **kwargs):
        serializer = serializers.CheckingSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        check_kyc_data.apply_async(args=[request.user.pk], kwargs=serializer.data)

        return Response(status=status.HTTP_202_ACCEPTED)
