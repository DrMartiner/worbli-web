from django.urls import path

from . import views

urlpatterns = [
    path('checking/', views.CheckingView.as_view(), name='checking'),
]
