import requests_mock
from django.conf import settings
from rest_framework.reverse import reverse

from apps.api.kyc.serializers import CheckingSerializer
from ...base_test import BaseApiTest


@requests_mock.Mocker()
class KycCheckingTest(BaseApiTest):
    url = reverse('api_v1:kyc:checking')

    post_data = {
        'usertoken': '123',
        'givenname': 'Smitt',
        'lastname': 'John',
        'passportno': '123456 789',
    }

    def setUp(self):
        super(KycCheckingTest, self).setUp()

        self.client.force_login(self.user)

    def test_success(self, m):
        for field in self._fields:
            self._mock_url(field, m, json={'is_right': True})

        response = self.client.post(self.url, data=self.post_data)
        self.assertResponseStatus202(response)

        self.reload_user_instance()
        # TODO: to check user profile

        # TODO: to check calling requests.post

    def _mock_url(self, m, field: str, **kwargs):
        url = self._get_url(field)
        return m.register_uri('POST', url, **kwargs)

    @property
    def _fields(self) -> list:
        fields = list(CheckingSerializer().fields.fields)
        fields.remove('usertoken')

        return fields

    def _get_url(self, field) -> str:
        return f"{settings.AUTHENTEQ['API_ROOT']}/api/v1/claims/{field}"
