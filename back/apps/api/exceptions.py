from rest_framework import status
from rest_framework.exceptions import APIException
from django.utils.translation import gettext_lazy as _


class ConflictError(APIException):
    status_code = status.HTTP_409_CONFLICT
    default_detail = _('Conflict error.')
    default_code = 'conflict'


class GoneError(APIException):
    status_code = status.HTTP_410_GONE
    default_detail = _('Gone error.')
    default_code = 'gone'

