from rest_framework.routers import DefaultRouter

from .views import DocumentModelViewSet

router = DefaultRouter()
router.register(r'', DocumentModelViewSet, base_name='document')

urlpatterns = router.urls
