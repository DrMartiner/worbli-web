from django_dynamic_fixture import G
from rest_framework.reverse import reverse

from apps.document.models import Document
from ...base_test import BaseApiTest
from .. import serializers


class DocumentReadListTest(BaseApiTest):
    def test_read_list(self):
        for i in range(3):
            G(Document)

        url = reverse('api_v1:document:document-list')
        response = self.client.get(url)
        self.assertResponseStatus200(response)

        self.assertListInResponse(serializers.DocumentSerializer, Document.objects.all(), response)
