from django_dynamic_fixture import G
from rest_framework.reverse import reverse

from apps.document.models import Document
from ...base_test import BaseApiTest
from .. import serializers


class DocumentReadDetailTest(BaseApiTest):
    def test_read_detail(self):
        instance = G(Document)

        url = reverse('api_v1:document:document-detail', args=[instance.pk])
        response = self.client.get(url)
        self.assertResponseStatus200(response)

        self.assertRetrieveInResponse(serializers.DocumentSerializer, instance, response)
