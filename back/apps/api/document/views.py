from rest_framework.viewsets import ReadOnlyModelViewSet

from apps.document.models import Document
from . import serializers


class DocumentModelViewSet(ReadOnlyModelViewSet):
    permission_classes = []
    queryset = Document.objects.all()
    serializer_class = serializers.DocumentSerializer
