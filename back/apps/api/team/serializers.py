from rest_framework import serializers

from apps.team import models


class EmployeeSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Employee
        fields = ['name', 'position', 'linkedin', 'image']


class DepartamentSerializer(serializers.ModelSerializer):
    employees = serializers.SerializerMethodField(read_only=True)

    def get_employees(self, obj: models.Departament) -> list:
        serializer = EmployeeSerializer(many=True, instance=obj.employee_set.all())

        return serializer.data

    class Meta:
        model = models.Departament
        fields = ['name', 'employees']
