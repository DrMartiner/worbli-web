from django.urls import path

from . import views


urlpatterns = [
    path('departament/', views.DepartamentListView.as_view(), name='departament-list'),
]
