from requests import Response
from rest_framework import generics
from rest_framework.views import APIView

from apps.team.models import Departament
from . import serializers


class DepartamentListView(generics.ListAPIView):
    permission_classes = []
    queryset = Departament.objects.all()
    serializer_class = serializers.DepartamentSerializer
