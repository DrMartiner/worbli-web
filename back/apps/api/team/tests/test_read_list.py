from django_dynamic_fixture import G
from rest_framework.reverse import reverse

from apps.team import models
from ...base_test import BaseApiTest
from .. import serializers


class DepartamentReadListTest(BaseApiTest):
    def test_read_list(self):
        for i in range(3):
            departament: models.Departament = G(models.Departament)
            for j in range(2):
                G(models.Employee, departament=departament)

        url = reverse('api_v1:team:departament-list')
        response = self.client.get(url)
        self.assertResponseStatus200(response)

        self.assertListInResponse(serializers.DepartamentSerializer, models.Departament.objects.all(), response)
