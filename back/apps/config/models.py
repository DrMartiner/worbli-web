from django.db import models
from solo.models import SingletonModel


class HomeConfig(SingletonModel):
    main_title = models.CharField(max_length=512, default='WORBLI’s 1 Billion Tokens Airdrop')
    main_text = models.TextField(default='“An Airdrop is the new ICO”. WORBLI is utilizing an Airdrop to launch the main network. 40% of WBI token supply is allocated to EOS Holders, register your email to claim for free!')
    main_video = models.URLField(blank=True, null=True, default='https://www.youtube.com/watch?v=LCauhHcWRgI')

    link_terms = models.URLField(blank=True, null=True)
    link_privacy = models.URLField(blank=True, null=True)

    class Meta:
        app_label = 'config'


class AboutUsConfig(SingletonModel):
    about_title = models.CharField(max_length=512, default='About WORBLI')
    about_text = models.TextField(default='WORBLI is a financial services network (FSN) where enterprises and individuals can access a broad range of services.')
    about_video = models.URLField(blank=True, null=True)

    our_title = models.CharField(max_length=512, default='Our Vision')
    our_text = models.TextField(default='We believe blockchain technology is going to considerably improve financial services, cloud computing, data analysis, security and storage, supply chain logistics, social networks, voting, healthcare, income inequality, accounting, contractual arrangements and so much more, and WORBLI will be at the forefront of product innovation.')

    block1_title = models.CharField(max_length=512, default='The WORBLI Foundation')
    block1_text = models.TextField(default='The WORBLI Foundation is a not for profit organisation that is committed to fulfilling our vision through the creation of a vibrant WORBLI community, with the following protocols:')

    block2_title = models.CharField(max_length=512, default='Compliant, Reliable and Secure')
    block2_text = models.TextField(default='WORBLI Network is being built especially for Fintech and Compliance focused products, services and businesses.')

    typical_application_title = models.CharField(max_length=512, default='Typical Applications on WORBLI Network')

    feature_title = models.CharField(max_length=512, default='Key Features of WORBLI')
    feature_sub_title = models.CharField(max_length=512, default='The future is near and we’re ready to take you there.')

    class Meta:
        app_label = 'config'


class DashboardConfig(SingletonModel):
    verify_message = models.TextField(default='To receive Sharedrop you need verify personal information')
    question_message = models.TextField(default='Got questions? Contact us at <a href="help@worbli.io">help@worbli.io</a> or ask in the Worbli on Telegram')

    class Meta:
        app_label = 'config'
