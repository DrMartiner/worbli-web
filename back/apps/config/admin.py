from django.contrib import admin
from solo.admin import SingletonModelAdmin
from django.utils.translation import ugettext_lazy as _

from . import models


@admin.register(models.HomeConfig)
class HomeConfigAdmin(SingletonModelAdmin):
    fieldsets = (
        (_('Main'), {'fields': ('main_title', 'main_text', 'main_video')}),
        (_('Links'), {'fields': ('link_terms', 'link_privacy')}),
    )


@admin.register(models.DashboardConfig)
class DashboardConfigAdmin(SingletonModelAdmin):
    fieldsets = (
        (_('Main'), {'fields': ('main_title', 'main_text', 'main_video')}),
        (_('Links'), {'fields': ('link_terms', 'link_privacy')}),
    )


@admin.register(models.AboutUsConfig)
class AboutUsConfigAdmin(SingletonModelAdmin):
    fieldsets = (
        (_('About'), {'fields': ('about_title', 'about_text', 'about_video')}),
        (_('Our vision'), {'fields': ('our_title', 'our_text',)}),
        (_('Block 1'), {'fields': ('block1_title', 'block1_text',)}),
        (_('Block 2'), {'fields': ('block2_title', 'block2_text',)}),
        (_('Application title'), {'fields': ('typical_application_title', )}),
        (_('Features'), {'fields': ('feature_title', 'feature_sub_title',)}),
    )
