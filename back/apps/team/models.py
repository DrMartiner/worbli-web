from django.db import models
from django.template.defaultfilters import truncatechars


class Departament(models.Model):
    name = models.CharField(max_length=256)

    order = models.PositiveIntegerField(default=0)

    def __str__(self):
        return truncatechars(self.name, 32)

    class Meta:
        app_label = 'team'
        ordering = ['order']


class Employee(models.Model):
    name = models.CharField(max_length=256)
    departament = models.ForeignKey('Departament', on_delete=models.CASCADE)
    position = models.CharField(max_length=128, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    linkedin = models.URLField(blank=True, null=True)
    image = models.ImageField(upload_to='employee')

    order = models.PositiveIntegerField(default=0)

    def __str__(self):
        return truncatechars(self.name, 32)

    class Meta:
        app_label = 'team'
        ordering = ['order']
