from django.contrib import admin

from adminsortable2.admin import SortableAdminMixin

from .models import Departament, Employee


@admin.register(Departament)
class DepartamentAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ['name']


@admin.register(Employee)
class EmployeeAdmin(SortableAdminMixin, admin.ModelAdmin):
    list_display = ['name', 'position', 'departament']
    list_filter = ['departament']
    search_fields = ['name', 'position']

    fieldsets = (
        ('Main', {'fields': ('name', 'departament', 'position', 'linkedin', 'image', 'description')}),
    )
