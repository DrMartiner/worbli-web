from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import Document


class DocumentAdminForm(forms.ModelForm):
    class Meta:
        model = Document
        fields = '__all__'

    def clean_file(self):
        is_file = self.cleaned_data.get('type') == Document.TYPE.FILE
        if is_file and self.cleaned_data.get('file') is None:
            raise forms.ValidationError(_('Field "file" can\'t be null for this type'))

        return self.cleaned_data['file']

    def clean_link(self):
        is_link = self.cleaned_data.get('type') == Document.TYPE.LINK
        if is_link and self.cleaned_data.get('link') is None:
            raise forms.ValidationError(_('Field "link" can\'t be null for this type'))

        return self.cleaned_data['link']
