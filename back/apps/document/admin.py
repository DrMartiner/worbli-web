from adminsortable2.admin import SortableAdminMixin
from django.contrib import admin

from .forms import DocumentAdminForm
from .models import Document


@admin.register(Document)
class DocumentAdmin(SortableAdminMixin, admin.ModelAdmin):
    form = DocumentAdminForm

    list_display = ['name', 'type']
    list_filter = ['type']
    search_fields = ['name', ]

    fieldsets = (
        ('Main', {'fields': ('name', 'type', ('file', 'link'))}),
    )
