from django.template.defaultfilters import truncatechars
from django.utils.translation import ugettext_lazy as _
from django.db import models


class Document(models.Model):
    class TYPE:
        FILE = 'file'
        LINK = 'link'

        CHOICES = (
            (FILE, _('File')),
            (LINK, _('Link')),
        )

    name = models.CharField(max_length=256)
    type = models.CharField(max_length=8, choices=TYPE.CHOICES)
    file = models.FileField(upload_to='documents', blank=True, null=True)
    link = models.URLField(blank=True, null=True)

    order = models.PositiveIntegerField(default=0)

    def __str__(self):
        return truncatechars(self.name, 32)

    class Meta:
        app_label = 'document'
        ordering = ['order']
