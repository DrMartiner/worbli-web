from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .forms import UserCreationForm, UserChangeForm

from .models import User


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    add_form = UserCreationForm
    form = UserChangeForm

    list_display = [
        'email', 'first_name', 'last_name', 'country',  'last_login', 'date_joined', 'is_verify', 'is_superuser', 'is_active',
    ]
    list_filter = ['last_login', 'date_joined']
    search_fields = ['email', 'first_name', 'last_name', ]
    list_editable = ['is_superuser', 'is_active', ]
    readonly_fields = [
        'sign_up_code', 'sign_up_code_created',
        'restore_password_code', 'restore_password_created',
        'is_givenname_verify', 'is_surname_verify', 'is_passportno_verify',
    ]

    fieldsets = (
        (None, {'fields': ('email', 'username', 'password',)}),
        (_('Personal info'), {'fields': ('country', 'first_name', 'last_name', )}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        (_('Sign-Up'), {'fields': ('sign_up_code', 'sign_up_code_created')}),
        (_('Restore password'), {'fields': ('restore_password_code', 'restore_password_created')}),
        (_('Authenteq'), {
            'fields': (
                'is_givenname_verify',
                'is_surname_verify',
                'is_passportno_verify',
            )
        }),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'username', 'password1', 'password2'),
        }),
    )
