import string
import random
from datetime import timedelta

from django.conf import settings
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django_countries.fields import CountryField
from django.db import models
from templated_email import send_templated_mail


def get_random_str() -> str:
    symbols = string.ascii_lowercase + string.digits
    return ''.join(random.choice(symbols) for _ in range(32))

User.objects.make_random_password

class User(AbstractUser):
    AUTHENTEQ_SCOPE = ['givenname', 'surname', 'passportno']

    USERNAME_FIELD: str = 'email'
    REQUIRED_FIELDS: type = ('username', )

    email = models.EmailField(_('email address'), unique=True)

    country = CountryField(blank=True, null=True)

    scatter_hash = models.CharField(max_length=101, blank=True, null=True, unique=True)

    sign_up_code = models.CharField(max_length=32, default=get_random_str)
    sign_up_code_created = models.DateTimeField(default=timezone.now)

    restore_password_code = models.CharField(max_length=32, default=make_random_password)
    restore_password_created = models.DateTimeField(default=timezone.now)

    is_givenname_verify = models.NullBooleanField()
    is_surname_verify = models.NullBooleanField()
    is_passportno_verify = models.NullBooleanField()

    @property
    def is_verify(self) -> bool:
        return all([getattr(self, f) for f in self.authenteq_fields_names])

    @property
    def authenteq_fields_names(self) -> list:
        return [f'is_{f}_verify' for f in self.AUTHENTEQ_SCOPE]

    @property
    def is_sign_up_code_expired(self):
        return timezone.now() > self.sign_up_code_created + timedelta(days=5)

    def update_sign_up_code(self):
        self.sign_up_code = get_random_str()
        self.sign_up_code_created = timezone.now()
        self.save()

    def send_sign_up_code(self, request):
        send_templated_mail(
            template_name='auth_sign_up_code',
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[self.email],
            context={'request': request, 'user': self}
        )

    @property
    def is_restore_password_code_expired(self):
        return timezone.now() > self.restore_password_created + timedelta(days=5)

    def update_restore_password_code(self):
        self.restore_password_code = get_random_str()
        self.restore_password_created = timezone.now()
        self.save()

    def send_restore_code(self, request):
        send_templated_mail(
            template_name='auth_restore_password_code',
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[self.email],
            context={'request': request, 'user': self}
        )

    def __str__(self):
        return self.email

    class Meta:
        app_label = 'users'
