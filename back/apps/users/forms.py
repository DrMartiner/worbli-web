from django.contrib.auth.forms import (
    UsernameField,
    UserCreationForm as BaseUserCreationForm,
    UserChangeForm as BaseUserChangeForm
)
from django_countries.widgets import CountrySelectWidget

from .models import User


class UserCreationForm(BaseUserCreationForm):
    class Meta(BaseUserCreationForm.Meta):
        model = User
        fields = ['email', 'username']
        field_classes = {'username': UsernameField}


class UserChangeForm(BaseUserChangeForm):
    class Meta(BaseUserChangeForm.Meta):
        widgets = {'country': CountrySelectWidget()}
