# To run
## Local demo
- run this
```bash
git clone git@gitlab.com:DrMartiner/worbli-web.git
cd worbli-web
echo "ROLE=demo" > .env
docker-compose -f docker-compose-demo.yml up -d nginx
```
- open in a browser (email: *demo@demo.com* & pass: *demo*): 
  * [Site](http://localhost:8080/ "Local demo site")
  * [Django Admin](http://localhost:8080/admin/django/ "Local demo admin")  
  * [Console Log](http://localhost:8080/admin/logs/ "Local demo Kibana logs")

Btw, u should waite 30-40 sec before open the ones at browser, because Django are need some times to up

## Stage server
- run this
```bash
git clone git@gitlab.com:DrMartiner/worbli-web.git
cd worbli-web
export ROLE=stage
docker-compose up -d nginx
```
- open in a browser [https://stage.worbli.net](http://localhost:8000/ "Local demo link")

# To develop

## Back-end
- ```export ROLE=debug```
- create configure file: ```back/.env.ini```
- define there:
```
DEBUG=on

BROKER_URL=redis://locahost:6379
CELERY_BROKER_URL=redis://locahost:6379
CELERY_RESULT_BACKEND_URL=redis://locahost:6379

POSTGRES_HOST=localhost
POSTGRES_DB=worblie
POSTGRES_USER=worblie
POSTGRES_PASSWORD=password
```

- run Redis & Postgres at your localhost 
- create database *worblie*, user *worblie*  with password *password*
- run this commands:

```bash
cd back
pip install pipenv
pipenv install --dev 

export ROLE=debug

./manage.py migrate
./manage.py loaddata project/fixtures/$ROLE/*
./manage.py runserver
```
- open [Django Admin](http://localhost:8000/admin/django/ "Local develop") and use *admin@admin.com* & *123* to login

### To run tests
```bash
export ROLE=test
./manage.py test
```

## Front-end
- run this commands:
Before npm install u should use Python 2.7.14 to correct install sha3@1.2.2.
U can use [PyEnv](https://github.com/pyenv/pyenv/ "PyEnv project")
```bash 
pyenv local 2.7.14
pyenv install 2.7.14
```

```bash
cd front
npm i
npm start # For calling to local django server
npm run start-stage  # For calling to stage server
```
- open [localhost:4200](http://localhost:4200/ "Local develop")
- at the end you should build your app via command: ```ng build```

# Diagrams
[Diagrams](./docs/diagrams/index.md)
